
.. _intro:

============
Introducción
============

.. admonition:: Course Overview

    Este curso trata acerca de base de datos y el uso de sistemas de gestion de
    base de datos (*Database Management System* [#db]_), desde el punto de
    vista del diseñador, usuario y desarrolador de aplicaciones de base de
    datos.

.. [#db] http://en.wikipedia.org/wiki/Database_management_system

.. _DBMS:

Database Management System
==========================

.. proporciona...
.. 
..     ... almacenamiento_ multi-usuario eficiente, confiable, conveniente y
..     seguro, y el acceso a grandes cantidades de datos persistentes,

.. admonition:: Provides...

    ... efficient_, reliable_, convenient, and safe multi-user_ storage of and
    access to massive_ amounts of persistent_ data.


.. _massive:

massive
    los sistemas de bases de datos manipulan datos a escala masiva (*massive
    scale*). Si pensamos las cantidades de datos producidas hoy en día, los
    DBMS_ manipulan, hasta, terabytes [#tera]_ de datos cada día.

    Un aspecto critcico de los datos que manipulan los DBMS_, es más mucho más
    grande que el que pued ingresarse en una memoria de un sistema de computo
    típico. Así la capacidad de las mmorias estan creciendo muy, muy rápido,
    pero los datos en el mundo y los datos manipulados por los DBMS_ crecen
    mucho mas rápido.

    **Los DBMS están diseñados para manipular data que reside fuera de la
    memoria**

.. [#tera] :math:`10^{12}` bytes. http://en.wikipedia.org/wiki/Terabyte

.. _persistent:

Persistent
    Los datos manipulados por los DBMS_ es tipicamente persistente [#per]_, lo
    que quiere decir que los datos *sobreviven* a los programas que se ejecutan en
    los datos. Por ejemplo, si ejecutas un programa de computadora, iniciara las
    variables que creamos, los datos estarán mientras el programa se ejecuta, y
    cuando el programa finalice los datos se *iran*. pero en las bases de datos
    los datos estan ahi, cuando el programa inicia y ejecuta sobre los datos y se detiene 
    los datos seguiran ahí. Frecuentemente, muchos programas estarán operando sobre
    los mismos datos.

.. [#per] http://en.wikipedia.org/wiki/Persistence_%28computer_science%29

.. _safe:

Safe
    Los DBMS_ ejecutan aplicaciones criticas tales como telecomuniccaciones y sistemas bancarios
    tienen que garantizar que los datos en la base de datos permanezcan en un
    estado consistente, no se perderán o sobreescribira conando ocurran fallas, y
    pueden haver fallas de hardware, software, cortes de energía, o usuarios maliciosos
    intentan corromper los datos.

    Asi los DBMS_ poseen un numero de mecanismos incluidos que aseguran que los datos permanezcan
    consistentes, sin importar lo que suceda.


.. _multi-user:

Multi-User
    Una aplicacio de usuario accede a los datos **concurrentemente**. Asi
    cuando tienes multiples aplicaciones trabajando sobre los mismos datosm el
    DBMS_, tiene que poseer los mecanismos para asegurarse que los datos
    permanezcan consistentes. Por ejemplo, para que no tengamos la mitad de un
    item manipulado por un usuario y la otra mitad sobreescrita por otro
    usuario,

    Asi, los DBMS_, tienen un sistema llamado control de concurrencia
    (*concurrency control*)m y la idea es que nosotros controlamos la manera
    como multiples usuarios acceden a la base de datos. Nosotros no controlamos
    con un solo usuario que tiene acceso exclusivo a ala base de datos o la perfomance
    descendera considerablemente, asi el control ocurre, actualmente, al nivel
    de items de datos en la base de datos. Muchos usuarios pueden manipular la misma 
    base de datos pero estaran operando en diferentes items de datos individuales.

.. _convenient:

Convenient
    Es una de las carateristicas criticas de la base de datos. Los DBMS_ estan diseñados
    para que sea fácial trabajar con grandes cantidades de datos y hacer pontentes
    y interesantes procesamient sobre los datos. Niveles:

    Physical Data Independence
        la manera en que los datos estan almacenados y dispuestos en los discos
        es independiente a la manera en que los programas *piensan en* la
        estructura del dato.

    High-Level Query Languages
        * Compacto
        * **Declarativo**: En el *query* describes lo que quieres de la base de
          datos, sin algoritmos.

.. _efficient:

efficient
    JOKE
        *En bases de datos, las tres cosas más importantes son la perfomance, en
        segundo la perfomance y por último perfomance*

    las bases de datos...
        - Miles de consultas (*querys*) por segundo

.. _reliable:

reliable
    - 99,9999 % uptime


Conceptos Claves
================

Data model
    Es una descripcion, en general, como los datos estan estructurados.

    * Set of records
    * XML -> jerarquico
    * graph -> nodos y vertices.

Schema versus data
    Schema
        * tipos (*types*) en lenguajes de programación
        * la estructura de la base de datos
        * se define al inicio y no cambia
        * DDL

    Data
        * variables en lenguajes de programación
        * datos almacenados dentro del schema
        * cambia rapidamente

Data Definition Language (DDL)
    * Configura el schema/estructura de la bse de datos.

Data Manipulation Language (DML)
    * Consultar y modificar la base de datos.

Personas claves
===============

personas involucradas en un sistemas de base de datos

BDMS Implementer
    persona que implementa el DBMS_ -> contruye el sistema [fuera de los
    alcances de este curso]

Database designer
    persona que establece el schema de la base de datos

Database Application Developer
    persona que construye una aplicacion o programa que se ejecutara sobre la
    base de datos.

Database Administrator
    persona que carga (*load*) el DBMS_ y se encarga de que permanezca
    ejecutandose.

    * trabajo interesante
    * se encarga del perfomace
    * bien pagado

.. important::

    Lo sepas o no, usamos bases de datos cada dia.  mejor dicho, cada hora.

