
=================
Well-formated XML
=================

Contrucciones básicas
=====================

Tagged elements (nested)
    Por ejemplo, a continuación el tag es ``first_name``:

    .. code-block:: xml

        <first_name>Mario</first_name>

Attributes
    Por ejemplo, a continuación el atributo es ``ISBN``:

    .. code-block:: xml

        <book ISBN="ISBN-0-13-666">
           <!-- some content -->
        </book>

Text
    Por ejemplo, a continuación el text el ``Database systems``

    .. code-block:: xml

        <title>Database systems<title>




Modelo Relacional versus XML
============================

=============== =================== ==============================
.               Relacional          XML
=============== =================== ==============================
Structure       Tables              Hierarchical, **Tree**, Graph
Schema          Fixed in advance    Flexible, "self-describing"
Queries         SQL, nice langs     Less so
Ordering        Unordered           Implied
Implementation  Native              Add-on
=============== =================== ==============================

* En el modelo relacional el *Schema* es absolumanete requerido, en XML es
  opcional

Well-formed
===========

requerimientos básicos
    * single root element
    * matched tags, proper nesting
    * Unique attributes within elements

eXtensible Markup Language (XML)
================================
* ... un estandar para la representacion y intecambio de datos
* ... competidor al modelo relacional
* ... más flexible que el modelo relacional
* ... la escepficicación formal de XML es enorme


