
=========================
Introduction to Databases
=========================

Apuntes *incompletos* de db-class_.

.. _db-class: http://www.db-class.org/

.. toctree::
   :maxdepth: 1

   intro
   relational_model
   querying
   well-formed_xml
   dtd

