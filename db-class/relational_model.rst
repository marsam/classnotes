
====================
The Relational Model
====================

* Tiene ~35 años
* fundamento de las bases de datos, y subyace en la mayoria de bases dedatos
  comerciales
* Es un modelo muy sencillo y extremadamente expresivo para realizar
  *preguntas* a ala base de datos.

* Existe eficientes implementaciones del modelo relacional.

Construciones básicas del modelo relacional
===========================================

.. important::

    Database = set of named relations_ (or **tables**)


.. _relations:

relation
    Una base de datos esta contituido por un conjunto de relaciones (*relations*)
    referidos como "tablas" (*tables*), los cuales tienen un nombre
    
    Por ejemplo la tabla ``Student`` (en singular)::

        Student
        +------+-------+--------+--------+
        |      |       |        |        |
        +======+=======+========+========+
        |      |       |        |        |
        +------+-------+--------+--------+
        |      |       |        |        |
        +------+-------+--------+--------+
                ...
        +------+-------+--------+--------+
        |      |       |        |        |
        +------+-------+--------+--------+




.. important::

    Each relation has a set of named attributes_ (or **columns**)

.. _attributes:

attributes
    Ejemplo en la tabla ``Student``, agregamos los atributos: *ID*, *name*,
    *GPA*, *Photo*::

        Student
        +------+-------+--------+--------+
        | ID   | name  | GPA    | Photo  |
        +======+=======+========+========+
        |      |       |        |        |
        +------+-------+--------+--------+
        |      |       |        |        |
        +------+-------+--------+--------+
                ...
        +------+-------+--------+--------+
        |      |       |        |        |
        +------+-------+--------+--------+


.. important::

    Each tuple_ (or **row**) has a value for each attribute.

.. _tuple:

tuple
    Por ejemplo en nuestra tabla ``Students``::

        Student
        +------+-------+--------+--------+
        | ID   | name  | GPA    | Photo  |
        +======+=======+========+========+
        | 123  | Amy   | 3.9    | (^_^)  |
        +------+-------+--------+--------+
        | 234  | Bob   | 3.4    | (T_T)  |
        +------+-------+--------+--------+
                ...
        +------+-------+--------+--------+
        |      |       |        |        |
        +------+-------+--------+--------+

.. important::

    Each attribute has a type_ (**domain**)

.. _type:

type
    Por ejemplo en nuestra table ``Student``::

        ID     --> integer
        name   --> string
        GPA    --> float
        photo  --> jpeg file

