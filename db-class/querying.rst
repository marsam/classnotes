
=============================
Querying Relational databases
=============================

Relational Algebra
==================

* **Formal**
* Simbolos griegos

ID of stundent with GPA > 3.7 applying to Stanford:

.. math::

    \pi_{ID} \sigma_{GPA>3.7 \wedge cName=\text{'Stanford'}} (Student \bowtie Apply)

SQL
===

**Actual/Implemented**

ID of stundent with GPA > 3.7 applying to Stanford:

.. code-block:: sql

    SELECT Student.ID
    FROM Student, Apply
    WHERE Student.ID=Apply.ID
    AND GPA>3.7 AND cName='Stanford'


.. important::

    Se recomienda aprender primero algebra relacional antes que SQL.
