
================
DTD, ID & IDREFs
================

"Valid XML"
===========

* Well-formed XML
* content-specific specification

Document type Descriptor (DTD)
------------------------------


XML Schema Descriptor (XSD)
---------------------------
Es un lenguaje para especificarlos tipos de los elementos, atributos, cuales
permiten anidacion (*nested*), ocurrencias. También atributaos especiales
como el ID y el IDREFs, que son una especie de *pointers* 

.. table:: DTD/XSD versus "Well-formed" XML

    ======================================= ===========================
    DTD/XSD                                 well-formed XML
    ======================================= ===========================
    Programas asumen una estructura         flexibilidad
    facilitan el CSS/XSL                    Editar DTD can be painful
    Permite el intercambio (especificación) Leer DTD can be painful
    Documentación
    Beneficios del "typing"
    ======================================= ===========================

Validando XML
=============
Para validar DTD, podemos usar xmllint_::

    $ xmllint --valid --noout example.xml

.. _xmllint: http://xmlsoft.org/
