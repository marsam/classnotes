
=============
Welcome to AI
=============

.. admonition:: Course Overview

    .. figure:: images/overview1.jpg
       :align: center
       :width: 500px

       Course Overview (by larvecode_)

    Objetivos
        * Enseñar las bases de Inteligencia Artificial
        * Estimular el interes en Inteligencia Artificial

    Estructura
        1. Videos
        2. Quizzes
        3. Answer videos
        4. Homework Assignment
        5. Exams

.. _agente:

Intelligent Agents
==================
El agente puede percibir el entorno (*environment*) a traves de sus sensores (*sensors*)
y puede modificar su estado mediate sus actuadores (*actuators*).

.. figure:: images/agent1.jpg
   :align: center
   :width: 500px

   Intelligent Agent (by larvecode_)

La principal pregunta en inteligencia artificial es la función que asigna los
sensores a los actuadores. Esto es llamado politica de control (*control
policy*)

.. figure:: images/agent2.jpg
   :align: center
   :width: 500px

   Agent Control Policy (by larvecode_)

A lo largo de esta clase aprenderemos cómo el agente_ puede hacer mejores
decisiones para transportar a sus actuadores, basandose en los datos del sensor.

Las decisiones llevan a cabo muchas, muchas veces, en un bucle del *feedback*
del entorno, decisiones de los agentes, interaccion de los actuadores con el
entorno y asi. Esto es llamado el ciclo de percepción-accion  (*perception
action cycle*).

.. figure:: images/agent3.jpg
   :align: center
   :width: 500px

   Perception Action Cycle (by larvecode_)

Aplicaciones de Inteligencia Artificial
=======================================

AI in finance
-------------

Existe un gran número de aplicaciones de inteligencia artificial en finanzas,
a menudo en la forma de toma de decisiones comerciales --en cuyo caso el agente
se llama agente de comercio(*trading agent*)--. Y el entorno podrían ser el
mercado de valores, el mercado de bonos o el mercado de materias primas,
también puede leer noticias en linea y seguir ciertos eventos. Y sus decisiones
son usualmente decisiones de compra o venta.

.. figure:: images/applications1.jpg
   :align: center
   :width: 500px

   AI in finance (by larvecode_)

AI in robotics
--------------

Hay una gran historia de inteligencia artificial con la robotica. hay
diferentes tipos de robot y como ellos se relacionan con el entorno mediante
sus sensores (camaras, micrófonos, sensores tactiles); y como ellos reaccionan
a su entorno (mover sus ruedas, brasos, etc.)

.. figure:: images/applications2.jpg
   :align: center
   :width: 500px

   AI in robotics (by larvecode_)



.. admonition:: Web crawlers

    Cuando surgió internet, los primeros *web crawlers* se llamaron *robots*, y
    para bloquearles el acceso a tu página web, hay un archivo ``robots.txt``.

    .. figure:: images/applications3.jpg
       :align: center
       :width: 250px

       web crawlers (by larvecode_)

.. figure:: images/applications4.jpg
   :align: center
   :width: 500px

   AI in robotics (by larvecode_)

AI in games
-----------
Hay dos maneras en la que los juegos usan inteligencia artificial:

.. admonition:: Play against you

    Una de las maneras es cuando el *game agent* juega contra ti, como si fuera
    un ser humano.

    .. figure:: images/applications5.jpg
       :align: center
       :width: 500px

       AI in games (by larvecode_)

.. admonition:: Make characters in game more believable

    .. figure:: images/applications6.jpg
       :align: center
       :width: 500px

       AI in games (by larvecode_)

AI in medicine
--------------

.. figure:: images/applications7.jpg
   :align: center
   :width: 500px

   AI in medicine (by larvecode_)

.. figure:: images/applications8.jpg
   :align: center
   :width: 500px

   AI in medicine (by larvecode_)

AI on the web
-------------

.. figure:: images/applications9.jpg
   :align: center
   :width: 500px

   AI on the web (by larvecode_)

.. figure:: images/applications10.jpg
   :align: center
   :width: 500px

   AI on the web (by larvecode_)

Terminología
============
Existen diferentes terminos que se usan para distinguir los problemas de
Inteligencia Artificial.

Fully vs. Partially Observable
------------------------------

.. figure:: images/terminology1.jpg
   :align: center
   :width: 500px

   fully vs. partially observable (by larvecode_)

.. figure:: images/terminology2.jpg
   :align: center
   :width: 500px

   partially observable (by larvecode_)

Deterministic vs. Stochastic
----------------------------

.. figure:: images/terminology3.jpg
   :align: center
   :width: 500px

   deterministic vs. stochastic (by larvecode_)

Discrete vs. Continuous
-----------------------

.. figure:: images/terminology4.jpg
   :align: center
   :width: 500px

   discrete vs. continuous (by larvecode_)

Benign vs. Adversorial
----------------------

.. figure:: images/terminology5.jpg
   :align: center
   :width: 500px

   benign vs. adversorial (by larvecode_)


.. glossary::

    environment
        fully observable (completamente observable)
            Cuando lo que el agente_ puede detectar en cualquier punto del tiempo
            es información suficiente  para tomar una decision optima.

        partially observable (parcialmente observable)
            Cuando el agente_ es conciente solamente de una parte de estado del
            :term:`environment`

        deterministic (determinista)
            Cuando las acciones de tu agente_ unicamente determina el resultado.

        stochastic (estocastico)
            cuando

        discrete
            Cuando

        continuous
            Cuando




AI and Uncertainty
==================

.. figure:: images/uncertainty1.jpg
   :align: center
   :width: 500px

   AI & Uncertainty (by larvecode_)

Machine Translation
===================

.. figure:: images/machine_trans.jpg
   :align: center
   :width: 500px

   Machine Translation (by larvecode_)



Summary
=======

.. links:
.. _larvecode: http://larvecode.tumblr.com/


