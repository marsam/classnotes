
===============================
Introduccion a Machine Learning
===============================

Definicion
==========

Machine Learning is ...

.. epigraph::

    ... a field of study that gives computers the ability to learn whitout bening explicitly programmed

    -- Arthur samuel (1959)


.. epigraph::


    ... well-posed Learning Problem: A computer program is said to learn from
    experience :math:`E` with respect to some task :math:`T` and some performance
    measure :math:`P`, if its performance on :math:`T`, as measured by :math:`P`,
    improves with experience :math:`E`.    

    -- Tom Mitchell (1998) 

Unsupervised learning
=====================

    ... Aqui esta el dataset ¿puedes encontrar alguna estructura el los datos?

