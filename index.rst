.. Class Notes documentation master file, created by
   sphinx-quickstart on Mon Apr 02 18:22:18 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mario Rodas' Class Notes
========================

.. admonition:: Disclaimer 

    Estos son mis apuntes personales de las diferentes clases_ en las que estoy inscrito.
    Solamente he tomado apuntes de las unidades que me parecian mas interesantes y/o considero merecían mayor profundidad.
    Aunque son unos apuntes en español, se nombraran los principales términos en inglés.

    Cualquier sugerencia: ``rodasmario2 <at> gmail <dot> com``

.. _clases: http://www.class-central.com/

Contents:

.. toctree::
   :maxdepth: 2

   ai-class/index
   ml-class/index
   db-class/index
   nlp-class/index




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

